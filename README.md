# Turtlebot

This group repositories contains code for Turtlebot3

The functionality is split into separate docker containers that comprise a functional docker stack.
Each container has it's own repository.



## Get the code

0. install invoke tool `pip install invoke`
1. clone `gitlab-profile` repo to `turtlebot` directory:  ` git clone git@gitlab.com:roxautomation/machines/turtlebot/gitlab-profile.git turtlebot`. 
2. `cd turtlebot`, clone group repos `invoke clone ssh` or `http`

## Run the stack

docker stack uses pre-built images (see `docker-compose.yml`)

`docker-compose up -d`

## Add microservice repo

Create a new project:

```sh
cruft create https://gitlab.com/roxautomation/templates/python-microservice
```


#!/bin/bash

# Stop on error
set -e

# Update and upgrade the system
echo "Updating and upgrading the system..."
sudo apt update
sudo apt upgrade -y

# Install dependencies
echo "Installing dependencies..."
sudo apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev

# Download Python 3.12 source code
echo "Downloading Python 3.12 source code..."
wget https://www.python.org/ftp/python/3.12.0/Python-3.12.0.tgz

# Extract the downloaded file
echo "Extracting Python 3.12..."
tar -xzvf Python-3.12.0.tgz
cd Python-3.12.0/

# Configure the build
echo "Configuring the build..."
./configure --enable-optimizations

# Compile and install
echo "Compiling and installing Python 3.12. This may take a while..."
sudo make altinstall

# Verify the installation
echo "Verifying Python 3.12 installation..."
/usr/local/bin/python3.12 -V

# Cleanup
echo "Cleaning up..."
cd ..
rm -rf Python-3.12.0
rm Python-3.12.0.tgz

echo "Python 3.12 installation is complete."

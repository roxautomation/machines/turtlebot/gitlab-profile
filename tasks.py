#!/usr/bin/python3
""" Invoke tasks for the turtlebot group of repos """

# pylint: disable=unused-argument

import os
import urllib.parse

import requests  # type: ignore
from invoke import task

GROUP_PATH = "roxautomation/machines/turtlebot"

IMAGES = {
    "joystick": "registry.gitlab.com/roxautomation/machines/turtlebot/joystick",
    "gamepad": "registry.gitlab.com/roxautomation/machines/turtlebot/gamepad",
    "mosquitto": "registry.gitlab.com/roxautomation/images/mosquitto",
}


def get_repo_urls(url_type="http"):
    """Get all the repos in the group"""
    # URL encode the full group path
    # Check if url_type is either "ssh" or "http"
    if url_type not in ["ssh", "http"]:
        raise ValueError("url_type must be either 'ssh' or 'http'")

    encoded_group_path = urllib.parse.quote_plus(GROUP_PATH)

    # GitLab API URL
    gitlab_api_url = "https://gitlab.com/api/v4/groups/"

    # Fetch project data
    response = requests.get(
        f"{gitlab_api_url}{encoded_group_path}/projects?include_subgroups=true",
        timeout=3,
    )

    if response.status_code != 200:
        raise requests.exceptions.HTTPError(
            f"Failed to fetch data from GitLab API. Status code: {response.status_code}"
        )

    projects = response.json()
    if not projects:
        print("No projects found in the group.")  # Debugging line
    else:
        print(f"Found {len(projects)} projects.")  # Debugging line

    # return [project["http_url_to_repo"] for project in projects]

    # remove "gitlab-profile" from the list
    ret = [
        project[f"{url_type}_url_to_repo"]
        for project in projects
        if project["name"] != "gitlab-profile"
    ]

    # Return SSH URLs
    return ret


@task(name="list")
def ls(ctx, url_type="ssh"):
    """list all repos in the group"""

    repo_urls = get_repo_urls(url_type)
    for url in repo_urls:
        print(url)


@task
def clone(ctx, url_type: str):
    """clone all repos in the group, except gitlab-profile. provide url_type as either ssh or http"""
    repo_urls = get_repo_urls(url_type)
    for url in repo_urls:
        try:
            print(f"Cloning {url}")
            ctx.run(f"git clone {url}")
        except Exception:  # pylint: disable=broad-except
            pass

    # turn off version control for root directory
    vcs_off(ctx)


def run_cmd_in_subdirs(ctx, cmd):
    """run a command in all subdirectories that are git repos"""
    for directory in os.listdir():
        if os.path.isdir(directory):
            # check if directory is a git repo
            if not os.path.isdir(os.path.join(directory, ".git")):
                continue
            print(f"---------------------{directory}---------------------")
            print(f">{cmd}")
            with ctx.cd(directory):
                ctx.run(cmd)


@task
def git(ctx, cmd):
    """run a git command in all subdirectories that are git repos"""
    run_cmd_in_subdirs(ctx, f"git {cmd}")


@task
def vcs_off(ctx):
    """turn off version control for root directory"""

    # check if root directory is a git repo
    if not os.path.isdir(".git"):
        return

    print("Turning off version control for root directory")
    print("to turn it back on, run `invoke vcs_on`")
    ctx.run("mv .git .git.off")


@task
def vcs_on(ctx):
    """turn on version control for root directory"""
    ctx.run("mv .git.off .git")


@task
def get_images(ctx):
    """get all requird pre-built images"""

    for name, image in IMAGES.items():
        print(f"Pulling {name}")
        ctx.run(f"docker pull {image}")
